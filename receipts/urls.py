from django.urls import path
from .views import ReceiptList, create_receipt, CategoryListView


urlpatterns = [
    path("", ReceiptList.as_view(), name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", CategoryListView.as_view(), name="category_list"),
    # path("accounts/", AccountList.as_view(), name="account_list"),
]
