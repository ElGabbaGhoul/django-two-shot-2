from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic import CreateView
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


from receipts.models import Receipt, ExpenseCategory
from receipts.forms import ReceiptForm


# @login_required(login_url="/accounts/login/")
# only if you have a function. for class, use
# loginrequiredmixin
class ReceiptList(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "views/list.html"
    context_object_name = "receiptlistcontext"

    def get_success_url(self):
        return reverse("homepage")


# ONLY NEED THIS IF YOURE USING A FUNCTION DUMMY
# CLASS GETS LOGINREQUIREDMIXIN
@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST" and ReceiptForm:
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(commit=False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")
    elif ReceiptForm:
        form = ReceiptForm()
    else:
        form = None
    context = {
        "form": form,
    }
    return render(request, "views/create.html", context)


class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    fields = ["name", "owner"]
    template_name = "views/category_list.html"
    context_object_name = "categorylistcontext"

    def get_success_url(self):
        return reverse("homepage")  # change these?


# class AccountList(LoginRequiredMixin, ListView):
#     model = Receipt
#     template_name = "views/account_list.html"
#     context_object_name = "accountlistcontext"

#     def get_success_url(self):
#         return reverse("homepage")  # change these?
